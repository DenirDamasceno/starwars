
export class UrlCollection {
  public static readonly FILM = 'https://swapi.co/api/films/';
  public static readonly PEOPLE = 'https://swapi.co/api/people/';

  public static readonly SPECIES = 'https://swapi.co/api/species/';
  public static readonly PLANETS = 'https://swapi.co/api/planets/';

}
