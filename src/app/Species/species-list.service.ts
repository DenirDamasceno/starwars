
import { Injectable } from '@angular/core';
import { Http, Response} from '@angular/http';
import { Species } from './Species';
import {Observable} from 'rxjs/Rx';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { UrlCollection } from '../Helpers/UrlCollection';
import { ObjectConverter } from '../Helpers/ObjectConverter';

@Injectable()
export class SpeciesService {
  // Resolve HTTP using the constructor
  constructor (
    private http: Http
  ) {}

  // public variable to expose
  public PAGESIZE = 10;
  public totalPage = 0;
  public count = 0;
  public currentPage = 1;
  public isNextable = false;
  public isPrevable = false;

  getSpecies(): Observable<Species[]> {
    const thisService = this;
    const objectConverter = new ObjectConverter();

    function mapSpeciesResponse(response: Response): Species[] {
      thisService.currentPage = 1;
      thisService.count = response.json().count;
      thisService.isNextable = response.json().next !== null;
      thisService.isPrevable = response.json().previous !== null;

      if (thisService.count > thisService.PAGESIZE) {
        thisService.totalPage = Math.ceil(thisService.count / thisService.PAGESIZE);
      } else {
        thisService.totalPage = 0;
      }
      // The response of the API has a results
      // property with the actual results
      return response.json().results.map(objectConverter.convertResponseToSpecies);
    }

    // ...using get request
    return this.http.get(UrlCollection.SPECIES)
      // ...and calling .json() on the response to return data
      .map(mapSpeciesResponse)
      //...errors if any
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));

  }

}
