import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  MatAutocompleteModule,
  MatCheckboxModule, MatDatepickerModule, MatFormFieldModule,
  MatInputModule, MatNativeDateModule, MatRadioModule, MatSelectModule,
  MatSlideToggleModule, MatTabsModule, MatSidenavModule, MatMenuModule, MatCardModule, MatToolbarModule, MatExpansionModule,
  MatButtonModule, MatListModule

} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

@NgModule({
  imports: [
    CommonModule
  ],
  exports: [
    MatToolbarModule, BrowserAnimationsModule, MatToolbarModule,
    MatInputModule, MatSlideToggleModule,
    MatFormFieldModule, MatRadioModule, MatTabsModule,
    MatDatepickerModule, MatNativeDateModule, MatListModule,
    MatCheckboxModule, MatExpansionModule, MatSidenavModule, MatCardModule,
    MatSelectModule, MatAutocompleteModule, MatCardModule, MatExpansionModule, MatButtonModule, MatMenuModule

  ],
})
export class AppMaterialModule { }
