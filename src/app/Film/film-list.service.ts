
import { Injectable }     from '@angular/core'
import { Http, Response} from '@angular/http'
import { Film }    from './Film'
import {Observable} from 'rxjs/Rx'

import 'rxjs/add/operator/map'
import 'rxjs/add/operator/catch'

import { UrlCollection } from '../Helpers/UrlCollection'
import { ObjectConverter } from '../Helpers/ObjectConverter'

@Injectable()
export class FilmService {
  constructor (
    private http: Http,
  ) {}

  // public variable to expose
  public PAGESIZE: number = 10
  public totalPage: number = 0
  public count: number = 0
  public currentPage: number = 1
  public isNextable: boolean = false
  public isPrevable: boolean = false

  getFilms() : Observable<Film[]> {

    let thisService = this
    let objectConverter = new ObjectConverter()

    function mapFilmResponse(response:Response): Film[]{
      thisService.currentPage = 1
      thisService.count = response.json().count
      thisService.isNextable = response.json().next !== null
      thisService.isPrevable = response.json().previous !== null

      if(thisService.count > thisService.PAGESIZE){
        thisService.totalPage = Math.ceil(thisService.count / thisService.PAGESIZE)
      }else{
        thisService.totalPage = 0
      }


      return response.json().results.map(objectConverter.convertResponseToFilm)
    }

    return this.http.get(UrlCollection.FILM)
      .map(mapFilmResponse)
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'))

  }

}
