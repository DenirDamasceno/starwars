
import { NgModule }from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { FilmListComponent } from './Film/film-list.component';
import { PeopleListComponent } from './People/people-list.component';
import { SpeciesListComponent } from './Species/species-list.component';


const appRoutes: Routes = [
  { path: '', redirectTo: 'people', pathMatch: 'full' },
  { path: 'film',  component: FilmListComponent},
  { path: 'people', component: PeopleListComponent },
  { path: 'species', component: SpeciesListComponent },

];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes, { useHash: true })
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
