
import { Injectable }     from '@angular/core';
import { Http, Response} from '@angular/http';
import {Observable} from 'rxjs/Rx';
import { People }    from './People';



import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { UrlCollection } from '../Helpers/UrlCollection';
import { ObjectConverter } from '../Helpers/ObjectConverter';

@Injectable()
export class PeopleService {
  constructor (
    private http: Http
  ) {}

  // public variable to expose
  public PAGESIZE = 10;
  public totalPage = 0;
  public count = 0;
  public currentPage = 1;
  public isNextable = false;
  public isPrevable = false;


  getPeople(): Observable<People[]> {
    const thisService = this;
    const objectConverter = new ObjectConverter();

    function mapPeopleResponse(response: Response): People[] {

      thisService.currentPage = 1;
      thisService.count = response.json().count;
      thisService.isNextable = response.json().next !== null;
      thisService.isPrevable = response.json().previous !== null;

      if (thisService.count > thisService.PAGESIZE) {
        thisService.totalPage = Math.ceil(thisService.count / thisService.PAGESIZE);
      } else {
        thisService.totalPage = 0;
      }


      return response.json().results.map(objectConverter.convertResponseToPeople);
    }

    return this.http.get(UrlCollection.PEOPLE)
      .map(mapPeopleResponse)
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));

  }

}
