
import { Component } from '@angular/core';

@Component({
  moduleId: module.id,
  selector: 'HeaderBlock',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent {
    headerTitle: string;
    userName: string;

    constructor() {
      this.headerTitle = 'StarWars';
      this.userName = 'Denir';
    }
}
