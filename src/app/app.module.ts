import { NgModule }from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule, JsonpModule } from '@angular/http';

import { AppRoutingModule } from './app.routing.module';

import { AppComponent }  from './app.component';
import { HeaderComponent } from './Header/header.component';
import { NavigationComponent } from './Navigation/navigation.component';
import { FoooterComponent } from './Footer/footer.component';
import { FilmListComponent } from './Film/film-list.component';
import { PeopleListComponent } from './People/people-list.component';
import { SpeciesListComponent } from './Species/species-list.component';

import { AngularFontAwesomeModule } from 'angular-font-awesome';


import { MatToolbarModule } from '@angular/material';
import { CommonModule } from '@angular/common';
import {
  MatAutocompleteModule, MatCheckboxModule, MatDatepickerModule, MatFormFieldModule, MatInputModule, MatNativeDateModule, MatRadioModule,
  MatSelectModule,
  MatSlideToggleModule,
  MatTabsModule
} from '@angular/material';


/*
 Service to get data
 */
import { FilmService } from './Film/film-list.service';
import { PeopleService } from './People/people-list.service';
import { SpeciesService } from './Species/species-list.service';


import { FilmTitlePipe } from './Pipe/film-title/film-title.pipe';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AppMaterialModule } from './app-material/app-material.module';
import { SidenavComponent } from './sidenav/sidenav.component'


@NgModule({

  imports: [
    BrowserModule,
    HttpModule,
    JsonpModule,
    AppRoutingModule,
    AppMaterialModule,
    AngularFontAwesomeModule


  ],

  declarations: [
    AppComponent,
    HeaderComponent,
    NavigationComponent,
    FoooterComponent,
    FilmListComponent,
    PeopleListComponent,
    SpeciesListComponent,

    // Custom Pipe
    FilmTitlePipe,

    SidenavComponent
  ],

  providers: [
    FilmService,
    PeopleService,
    SpeciesService,

  ],

  bootstrap: [ AppComponent ]
})
export class AppModule { }
